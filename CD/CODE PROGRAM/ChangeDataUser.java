package id.madhang.apps;

import android.app.AlertDialog;
import android.support.test.espresso.Espresso;
import android.support.test.espresso.IdlingResource;
import android.support.test.espresso.PerformException;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.contrib.PickerActions;
import android.support.test.espresso.util.HumanReadables;
import android.support.test.espresso.util.TreeIterables;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.DatePicker;

import org.hamcrest.Matcher;
import org.hamcrest.Matchers;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import java.util.concurrent.TimeoutException;

import id.madhang.apps.Activities.ProfileActivity;
import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.isDialog;
import static android.support.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withClassName;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.core.AllOf.allOf;




/**
 * Created by JalalKun on 31/07/2018.
 */
 
 /**
 *Activity ini digunakan untuk melakukan pengujian pada skenario merubah data pengguna
 */
@RunWith(AndroidJUnit4.class)
public class ChangeDataUser {
	/**
	*Dalam @Rule di bawah digunakan untuk menentukan activity mana yang akan dilakukan pengujian
	*/
    @Rule
    public ActivityTestRule<ProfileActivity> profileActivityActivityTestRule = new ActivityTestRule<>(ProfileActivity.class);

	/**
	*Fungsi waitFor() digunakan untuk melakukan pause terhadap pengujian yang dilakukan, parameter yang digunakan dihitung dalam milidetik
	*/
    public static ViewAction waitFor(final long millis) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isRoot();
            }
	
            @Override
            public String getDescription() {
                return "Wait for " + millis + " milliseconds.";
            }

            @Override
            public void perform(UiController uiController, final View view) {
                uiController.loopMainThreadForAtLeast(millis);
            }
        };
    }
	/**
	*Di bawah ini lah pengujian skenario merubah data pengguna dilakukan
	*/
    @Test
    public void testChangeDataProfile() {
        String bank_name = "MANDIRI";
        String bank_number = "09187651022";
        String phone = "085640018411";
        int waiting_time = 3000;
        onView(isRoot()).perform(waitFor(waiting_time));
        onView(withId(R.id.layrek)).check(matches(isDisplayed())).perform(click());
        onView(withId(R.id.norekening)).check(matches(isDisplayed()));
        onView(withId(R.id.spinnerBank)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is(bank_name))).inRoot(isPlatformPopup()).perform(click());
        onView(withId(R.id.norekening)).perform(replaceText(bank_number));
        onView(withText("OK")).inRoot(isDialog()).check(matches(isDisplayed())).perform(click());
        onView(isRoot()).perform(waitFor(waiting_time));
        onView(withId(R.id.layphone)).check(matches(isDisplayed())).perform(click());
        onView(withId(R.id.inputan)).check(matches(isDisplayed())).perform(replaceText(phone));
        onView(withText("OK")).inRoot(isDialog()).check(matches(isDisplayed())).perform(click());
        onView(isRoot()).perform(waitFor(waiting_time));
        onView(withId(R.id.laygen)).check(matches(isDisplayed())).perform(click());
        onView(withId(R.id.radioPerempuan)).perform(click());
        onView(withText("OK")).inRoot(isDialog()).check(matches(isDisplayed())).perform(click());
        onView(isRoot()).perform(waitFor(waiting_time));
        onView(withId(R.id.laybirth)).check(matches(isDisplayed())).perform(click());
        onView(withId(R.id.inputan)).check(matches(isDisplayed())).perform(click());
        onView(withClassName(Matchers.equalTo(DatePicker.class.getName()))).perform(PickerActions.setDate(1995, 6, 4));
        onView(withText("Selesai")).perform(click());
        onView(withText("OK")).inRoot(isDialog()).check(matches(isDisplayed())).perform(click());
        onView(isRoot()).perform(waitFor(waiting_time));
        onView(withId(R.id.layprov)).check(matches(isDisplayed())).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("Sumatera Utara"))).perform(click());
        onView(isRoot()).perform(waitFor(waiting_time));
        onView(withId(R.id.laycity)).check(matches(isDisplayed())).perform(scrollTo());
        onView(withId(R.id.laycity)).perform(click());
        onData(allOf(is(instanceOf(String.class)), is("Kota Medan"))).perform(click());

    }
}
