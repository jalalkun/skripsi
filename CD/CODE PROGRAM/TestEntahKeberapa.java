package id.madhang.apps;

import android.app.Activity;
import android.app.Instrumentation;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.rule.ActivityTestRule;
import android.view.View;

import org.hamcrest.Matcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import id.madhang.apps.Activities.VerifikasiKTPActivity;

import static android.support.test.espresso.Espresso.closeSoftKeyboard;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.Intents.intending;
import static android.support.test.espresso.intent.Intents.times;
import static android.support.test.espresso.intent.matcher.IntentMatchers.toPackage;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.isRoot;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static id.madhang.apps.ImageViewMatcher.hasDrawable;

/**
 * Created by JalalKun on 28/07/2018.
 */
 
 /**
 *Activity ini digunakan untuk melakukan pengujian pada skenario verifikasi KTP
 */
 
@RunWith(JUnit4.class)
public class TestEntahKeberapa {
	/**
	*Dalam @Rule di bawah digunakan untuk menentukan activity mana yang akan dilakukan pengujian
	*/
    @Rule
    public IntentsTestRule<VerifikasiKTPActivity> ktpActivityIntentsTestRule = new IntentsTestRule<>(VerifikasiKTPActivity.class);

	/**
	*Fungsi waitFor() digunakan untuk melakukan pause terhadap pengujian yang dilakukan, parameter yang digunakan dihitung dalam milidetik
	*/
    public static ViewAction waitFor(final long millis) {
        return new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isRoot();
            }

            @Override
            public String getDescription() {
                return "Wait for " + millis + " milliseconds.";
            }

            @Override
            public void perform(UiController uiController, final View view) {
                uiController.loopMainThreadForAtLeast(millis);
            }
        };
    }

	/**
	*Di bawah ini lah pengujian skenario Verifikasi KTP dilakukan
	*/
    @Test
    public void setKtpActivityActivityTestRule(){
        int waiting_time = 3000;
        onView(isRoot()).perform(waitFor(waiting_time));
        onView(withId(R.id.content_belum_upload_ktp)).check(matches(isDisplayed()));
        onView(withId(R.id.btn_verifikasi_ktp_kirim_sekarang)).perform(click());
        onView(withId(R.id.content_no_ktp)).check(matches(isDisplayed()));
        onView(withId(R.id.et_no_ktp)).perform(typeText("1234567890123456"));
        closeSoftKeyboard();
        onView(withId(R.id.btn_verifikasi_ktp_no_berikutnya)).perform(click());
        onView(isRoot()).perform(waitFor(waiting_time));
        onView(withId(R.id.content_foto_ktp)).check(matches(isDisplayed()));
        Bitmap icon = BitmapFactory.decodeResource(
                InstrumentationRegistry.getTargetContext().getResources(),
                R.mipmap.ic_launcher);
        // Build a result to return from the Camera app
        Intent resultData = new Intent();
        resultData.putExtra("data", icon);
        Instrumentation.ActivityResult result = new Instrumentation.ActivityResult(Activity.RESULT_OK, resultData);
        // Stub out the Camera. When an intent is sent to the Camera, this tells Espresso to respond
        // with the ActivityResult we just created
        intending(toPackage("com.android.camera")).respondWith(result);
        onView(withId(R.id.ib_foto_ktp)).perform(click());
        onView(withId(R.id.ib_foto_ktp)).check(matches(hasDrawable()));
        onView(withId(R.id.btn_verifikasi_ktp_foto_berikutnya)).perform(click());
        onView(isRoot()).perform(waitFor(waiting_time));
        onView(withId(R.id.content_foto_diri_dengan_ktp)).check(matches(isDisplayed()));
        onView(withId(R.id.ib_foto_diri_ktp)).perform(click());
        intended(toPackage("com.android.camera"), times(2));
        onView(withId(R.id.btn_verifikasi_ktp_diri_berikutnya)).perform(click());
        onView(withId(R.id.content_peninjauan_ktp)).check(matches(isDisplayed()));
    }
}
